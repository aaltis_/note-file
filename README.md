# Idea #

Projects inspiration comes from Owncloud (http://owncloud.org/). Owncloud allows user to use his own computer as data storage cloud with simple and nice web and desktop client. Work was delimited to move files and notes between client and server supporting multiple users.

# QT #

Qt is Nokia's leftovers from better times. Its platform independent development environment for graphic user interfaces. Native c++, works also with C#, C#, Java, Python, Ruby and PHP.

# Python Flask #

Flask is microframework for python. Usually used with small webpages backend but is now twisted to work with QT client. Handles Json/http commands, parses those to database and uploads/downloads files. Works also as initializator for database.

# Json HTTP #

Json is used to communicate between client and backend. Http takes care of file upload and download.